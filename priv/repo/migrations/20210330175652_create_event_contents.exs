defmodule Plasma.Repo.Migrations.CreateEventContents do
  use Ecto.Migration

  def change do
    create table(:event_contents) do
      add :id, :uuid, primary_key: true
      add :content, :map
      add :event_id, references(:events, type: :uuid)
      timestamps()
    end
  end
end
