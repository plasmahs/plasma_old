defmodule Plasma.Repo.Migrations.CreateRooms do
  use Ecto.Migration

  def change do
    create table(:rooms) do
      add :id, :uuid, primary_key: true
      add :room_id, :text, null: false
      add :visibility, :string, null: false
      add :version, :string, null: false
      add :stateset_version, :integer
      timestamps()
    end

    create unique_index(:rooms, [:room_id])
  end
end
