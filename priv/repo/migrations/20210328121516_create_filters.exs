defmodule Plasma.Repo.Migrations.CreateFilters do
  use Ecto.Migration

  def change do
    create table(:filters) do
      add :id, :uuid, primary_key: true
      add :account_id, references(:accounts, type: :uuid)
      add :definition, :map
      timestamps()
    end
  end
end
