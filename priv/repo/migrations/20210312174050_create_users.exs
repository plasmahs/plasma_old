defmodule Plasma.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :id, :uuid, primary_key: true
      add :user_id, :text, null: false
      add :display_name, :text
      add :avatar_url, :text
      timestamps()
    end

    create unique_index(:users, [:user_id])
  end
end
