## Plasma configuration for development

This samples provides prerequisites for running plasma in developer environment. 
It provides:
 - a PostgreSQL database
 - a Nginx reverse proxy for exposing Riot and plasma through HTTPS

### Configuration

#### Database

Development requires two PostgreSQL database.
 1. one for storing Plasma data
 2. one for storing Synapse test instance
 
Before runnning Plasma and Synapse, you need to create the appropriate PostgreSQL database:

```

$ docker-compose exec db psql -U postgres postgres
postgres=# CREATE USER plasma WITH ENCRYPTED PASSWORD 'plasma';
postgres=# CREATE DATABASE plasma_dev OWNER plasma;
postgres=# CREATE USER synapse WITH ENCRYPTED PASSWORD 'synapse';
postgres=# CREATE DATABASE synapse ENCODING 'UTF8' LC_COLLATE='C' LC_CTYPE='C' template=template0 OWNER synapse;
$ mix ecto.create
$ mix ecto.migrate
```

#### TLS certificates

Both synapse and plasma requires TLS secured connection, so you will need valid certificates for running both servers.
Because auto-signed certificates can be painful, the suggested solution is to use [mkcert](https://github.com/FiloSottile/mkcert) to get locally trusted certificates.

Once you have installed mkcert, use the following commands to install the RootCA and create two certificates:

```
mkcert -install
cd ssl_config
mkcert plasma
cd -
cd synapse
mkcert synapse
```

Synapse and nginx reverse proxy are already configures to use the files.

Finally, add the following line to you `/etc/hosts` file:

```
127.0.0.1  synapse
127.0.0.1  plasma
```


#### Plasma 
See `plasma.yaml`

### Running

```
docker-compose up -d
```

Then run ...

