defmodule Plasma.Matrix.ClientApi.Controllers.R0.RegisterControllerTest do
  use Plasma.Matrix.ClientApi.ConnCase
  import Plasma.Factory
  alias Polyjuice.Util.Identifiers.V1.UserIdentifier

  describe "Registration" do
    test "returns 400/M_INVALID_USERNAME for invalid username", %{conn: conn} do
      session = get_auth_session(conn)

      request = %{
        "auth" => %{"type" => "m.login.dummy", "session" => session},
        "username" => "INVALID"
      }

      response =
        build_conn()
        |> put_req_header("content-type", "application/json")
        |> post(Routes.register_path(conn, :register), request)
        |> json_response(400)

      assert %{"errcode" => "M_INVALID_USERNAME"} == response
    end

    test "returns 400/M_USER_IN_USE for already in use username", %{conn: conn} do
      session = get_auth_session(conn)
      user = insert(:user)

      {:ok, %{localpart: localpart}} = UserIdentifier.parse(user.user_id)

      request = %{
        "auth" => %{"type" => "m.login.dummy", "session" => session},
        "username" => localpart
      }

      response =
        build_conn()
        |> put_req_header("content-type", "application/json")
        |> post(Routes.register_path(conn, :register), request)
        |> json_response(400)

      assert %{"errcode" => "M_USER_IN_USE"} == response
    end

    test "returns 200 with user response when registration succeed", %{
      conn: conn
    } do
      session = get_auth_session(conn)

      request = %{
        "auth" => %{"type" => "m.login.dummy", "session" => session},
        "username" => "someuser",
        "password" => "PaSSw0rd"
      }

      response =
        build_conn()
        |> put_req_header("content-type", "application/json")
        |> post(Routes.register_path(conn, :register), request)
        |> json_response(200)

      assert %{
               "access_token" => access_token,
               "user_id" => user_id,
               "device_id" => device_id
             } = response

      assert !is_nil(access_token)
      assert !is_nil(user_id)
      assert !is_nil(device_id)
    end

    test "returns 200 with user response with inhibit_login", %{conn: conn} do
      session = get_auth_session(conn)

      request = %{
        "auth" => %{"type" => "m.login.dummy", "session" => session},
        "username" => "someuser",
        "password" => "PaSSw0rd",
        "inhibit_login" => true
      }

      response =
        build_conn()
        |> put_req_header("content-type", "application/json")
        |> post(Routes.register_path(conn, :register), request)
        |> json_response(200)

      assert %{"user_id" => user_id, "device_id" => device_id} = response

      assert !is_nil(user_id)
      assert !is_nil(device_id)
    end

    test "available return username already used", %{
      conn: conn
    } do
      # First register some user
      session = get_auth_session(conn)

      request = %{
        "auth" => %{"type" => "m.login.dummy", "session" => session},
        "username" => "someuser",
        "password" => "PaSSw0rd"
      }

      build_conn()
      |> put_req_header("content-type", "application/json")
      |> post(Routes.register_path(conn, :register), request)
      |> json_response(200)

      assert %{"errcode" => "M_USER_IN_USE"} =
               build_conn()
               |> get(Routes.register_path(conn, :available), %{
                 "username" => "someuser"
               })
               |> json_response(400)
    end

    test "available return true for available username", %{
      conn: conn
    } do
      assert %{"available" => true} =
               build_conn()
               |> get(Routes.register_path(conn, :available), %{
                 "username" => "availableuser"
               })
               |> json_response(200)
    end
  end
end
