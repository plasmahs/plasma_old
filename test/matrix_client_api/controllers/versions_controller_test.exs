defmodule Plasma.Matrix.ClientApi.Controllers.VersionsControllerTest do
  use Plasma.Matrix.ClientApi.ConnCase

  test "GET /_matrix/client/versions returns current supported versions", %{
    conn: conn
  } do
    response =
      conn
      |> get(Routes.versions_path(conn, :get))
      |> json_response(200)

    assert response == %{"versions" => ["r0.6.1"]}
  end
end
