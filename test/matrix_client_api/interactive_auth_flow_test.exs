defmodule Plasma.Matrix.ClientApi.Plugs.InteractiveAuthFlowTest do
  use Plasma.Matrix.ClientApi.ConnCase

  describe "InteractiveAuthFlow" do
    test "return new auth flow response", %{conn: conn} do
      body =
        conn
        |> put_req_header("content-type", "application/json")
        |> post(Routes.register_path(conn, :register))
        |> json_response(401)

      assert String.length(String.trim(body["session"])) > 0
    end

    test "return new auth flow response for unknown session", %{conn: conn} do
      body =
        conn
        |> put_req_header("content-type", "application/json")
        |> post(Routes.register_path(conn, :register), %{
          "auth" => %{"session" => "TEST_SESSION"}
        })
        |> json_response(401)

      assert String.length(String.trim(body["session"])) > 0
      assert body["session"] != "TEST_SESSION"
    end

    test "return same auth flow response for existing session", %{conn: conn} do
      session = get_auth_session(conn)

      conn =
        build_conn()
        |> put_req_header("content-type", "application/json")
        |> post(Routes.register_path(conn, :register), %{
          "auth" => %{"session" => session}
        })

      body = json_response(conn, 401)
      assert String.length(String.trim(body["session"])) > 0
      assert body["session"] == session
    end

    test "complete auth flow with m.login.dummy", %{conn: conn} do
      session = get_auth_session(conn)
      # Test call to /register with invalid username, so we expect an error
      request = %{
        "auth" => %{"type" => "m.login.dummy", "session" => session},
        "username" => "INVALID"
      }

      response =
        build_conn()
        |> put_req_header("content-type", "application/json")
        |> post(Routes.register_path(conn, :register), request)
        |> json_response(400)

      assert %{"errcode" => "M_INVALID_USERNAME"} == response
    end
  end
end
