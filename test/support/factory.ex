defmodule Plasma.Factory do
  use ExMachina.Ecto, repo: Plasma.Repo

  def user_factory() do
    %Plasma.Users.User{
      user_id: sequence(:user_id, &"@user#{&1}:localhost")
    }
  end

  def account_factory() do
    %Plasma.Users.Account{
      password: "password",
      kind: "user",
      user: build(:user)
    }
  end

  def device_factory() do
    %Plasma.Users.Device{
      device_id: sequence(:device_id, &"device_#{&1}"),
      access_token: sequence(:access_token, &"access_token_#{&1}"),
      account: build(:account)
    }
  end
end
