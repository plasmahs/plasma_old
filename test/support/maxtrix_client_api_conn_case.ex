defmodule Plasma.Matrix.ClientApi.ConnCase do
  @moduledoc """
  This module defines the test case to be used by
  tests that require setting up a connection.

  Such tests rely on `Phoenix.ConnTest` and also
  import other functionality to make it easier
  to build common data structures and query the data layer.

  Finally, if the test case interacts with the database,
  we enable the SQL sandbox, so changes done to the database
  are reverted at the end of every test. If you are using
  PostgreSQL, you can even run database tests asynchronously
  by setting `use PlasmaWeb.ConnCase, async: true`, although
  this option is not recommended for other databases.
  """

  use ExUnit.CaseTemplate

  using do
    quote do
      # Import conveniences for testing with connections
      import Plug.Conn
      import Phoenix.ConnTest
      import Plasma.Matrix.ClientApi.ConnCase

      alias Plasma.Matrix.ClientApi.Router.Helpers, as: Routes

      # The default endpoint for testing
      @endpoint Plasma.Matrix.ClientApi.Endpoint

      def get_auth_session(conn) do
        %{"session" => session} =
          conn
          |> put_req_header("content-type", "application/json")
          |> post(Routes.register_path(conn, :register))
          |> json_response(401)

        session
      end
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Plasma.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(Plasma.Repo, {:shared, self()})
    end

    {:ok, conn: Phoenix.ConnTest.build_conn()}
  end
end
