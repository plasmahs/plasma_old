# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :plasma,
  ecto_repos: [Plasma.Repo]

# Don't autogenerate primary keys
config :plasma, Plasma.Repo,
  migration_primary_key: false,
  migration_timestamps: [type: :timestamptz]

# Configures the endpoint
config :plasma, PlasmaWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base:
    "A9t0rD+oZ1zfR9GKJtH+HN3x3VJif0mBB0LUl7rsvRnFLVDpr8KnOk2IseoW9JBt",
  render_errors: [view: PlasmaWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: Plasma.PubSub,
  live_view: [signing_salt: "5AIYkuv+"]

# Configures the endpoint
config :plasma, Plasma.Matrix.ClientApi.Endpoint,
  url: [host: "localhost"],
  secret_key_base:
    "A9t0rD+oZ1zfR9GKJtH+HN3x3VJif0mBB0LUl7rsvRnFLVDpr8KnOk2IseoW9JBt",
  render_errors: [
    view: Plasma.Matrix.ClientApi.ErrorView,
    accepts: ~w(json),
    layout: false
  ],
  pubsub_server: Plasma.PubSub,
  live_view: [signing_salt: "5AIYkuv+"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Oban config
config :plasma, Oban,
  repo: Plasma.Repo,
  plugins: [Oban.Plugins.Pruner],
  queues: [default: 10, rooms: 10]

# Distributed Cache
config :plasma, Plasma.Cache,
  model: :inclusive,
  levels: [
    {
      Plasma.Cache.L1,
      gc_interval: :timer.hours(1),
      allocated_memory: 2_000_000_000,
      gc_cleanup_min_timeout: :timer.seconds(10),
      gc_cleanup_max_timeout: :timer.minutes(10),
      backend: :shards
    },
    {
      Plasma.Cache.L2,
      primary: [
        gc_interval: :timer.hours(1),
        allocated_memory: 2_000_000_000,
        gc_cleanup_min_timeout: :timer.seconds(10),
        gc_cleanup_max_timeout: :timer.minutes(10),
        backend: :shards
      ]
    }
  ]

config :plasma, :runtime_defaults,
  config: %{password_config: %{"enabled" => "true"}, server_name: "localhost"}

config :plasma,
  access_token_length: 256

config :plasma, :matrix,
  supported_room_versions: ~w(1 2 3 4 5 6),
  default_room_version: "6"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
