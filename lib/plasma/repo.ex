defmodule Plasma.Repo do
  use Ecto.Repo,
    otp_app: :plasma,
    adapter: Ecto.Adapters.Postgres

  require Logger

  def init(_type, config) do
    case Plasma.Application.get_runtime_config() do
      {:ok, %{database: databaseConfig}} ->
        config
        |> Keyword.put(:username, databaseConfig["user"])
        |> Keyword.put(:password, databaseConfig["password"])
        |> Keyword.put(:database, databaseConfig["database"])
        |> Keyword.put(:hostname, databaseConfig["host"])
        |> (&{:ok, &1}).()

      _ ->
        {:ok, config}
    end
  end

  @doc """
  map a changeset error list to a list of `{field, error}` where `field` is the field on error
  and `error` the associated error
  """
  @spec changeset_fields_on_error(Ecto.Changeset.t()) :: [Keyword.t()]
  def changeset_fields_on_error(%Ecto.Changeset{errors: errors}) do
    errors |> Enum.map(fn {field, {error, _}} -> {field, error} end)
  end

  @doc """
  Get the error message associated with a `field` on error
  """
  @spec changeset_field_error(Ecto.Changeset.t(), atom()) :: String.t() | nil
  def changeset_field_error(changeset, field) do
    changeset
    |> changeset_fields_on_error
    |> Keyword.get(field, nil)
  end
end
