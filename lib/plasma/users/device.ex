defmodule Plasma.Users.Device do
  use Plasma.Schema
  alias Plasma.Users.Account

  @type t :: %__MODULE__{
          device_id: String.t(),
          display_name: String.t(),
          access_token: String.t(),
          account: Account.t(),
          last_seen: DateTime.t()
        }

  schema "devices" do
    field(:device_id, :string)
    field(:display_name, :string)
    field(:access_token, :string)
    belongs_to(:account, Account)
    field(:last_seen, :utc_datetime)
    timestamps()
  end

  def changeset(device, attrs \\ %{}) do
    device
    |> cast(attrs, [:device_id, :display_name, :access_token])
    |> validate_required([:device_id])
    |> assoc_constraint(:account)
  end

  def create_changeset(device, attrs \\ %{}), do: changeset(device, attrs)

  def update_changeset(device, attrs \\ %{}) do
    changeset(device, attrs)
  end
end
