defmodule Plasma.Users.Accounts do
  use Nebulex.Caching

  @moduledoc """
  Accounts related business logic
  """
  alias Plasma.Repo
  alias Plasma.Users.Account
  alias Plasma.Users.User
  alias Plasma.Users.Device
  alias Plasma.Users.Filter
  import Ecto.Query
  alias Ecto.Multi
  alias Polyjuice.Util.Identifiers.V1.UserIdentifier

  @spec register(map) ::
          {:ok, %{account: Account.t(), user: User.t(), device: Device.t()}}
          | {:error, String.t()}
          | {:invalid_user_id}
          | {:user_id_already_exists}

  def register(params) do
    with {:server_name, server_name} <- Plasma.RuntimeConfig.get_server_name(),
         {:ok, user_id} <- gen_user_identifier(params, server_name),
         request = Map.put(params, :user_id, "#{user_id}") do
      user_account_multi =
        Multi.new()
        |> Multi.insert(:user, User.create_changeset(%User{}, request))
        |> Multi.insert(:account, fn %{user: user} ->
          Ecto.build_assoc(user, :account) |> Account.create_changeset(request)
        end)

      res =
        user_account_multi
        |> Multi.merge(fn %{account: account} ->
          Multi.new()
          |> Multi.run(:device, fn repo, %{} ->
            case repo.get_by(Device, device_id: request.device_id) do
              nil ->
                Ecto.build_assoc(account, :devices)
                |> Device.create_changeset(request)
                |> repo.insert()

              device ->
                {:ok, device}
            end
          end)
        end)
        |> Repo.transaction()

      case res do
        {:error, :user, changeset, _} ->
          case Repo.changeset_field_error(changeset, :user_id) do
            "user_id_already_exists" -> {:user_id_already_exists}
          end

        {:ok, %{account: account, user: user, device: device}} ->
          {:ok, %{account: account, user: user, device: device}}
      end
    else
      {:error, :invalid_user_id} ->
        {:invalid_user_id}

      {:error, message} ->
        {:error, message}
    end
  end

  defp gen_user_identifier(params, server_name) do
    case params.username do
      nil -> {:ok, UserIdentifier.generate(server_name)}
      username -> UserIdentifier.new(username, server_name)
    end
  end

  defp account_by_user_id_query_with_preload(user_id) do
    from(a in Account,
      join: u in assoc(a, :user),
      where: u.user_id == ^user_id,
      preload: [:user, :devices]
    )

    # from a in Account,
    #  join: u in User,
    #  on: a.user_id == u.id,
    #  where: u.user_id == ^user_id
  end

  @ttl :timer.hours(1)
  @decorate cacheable(
              cache: Plasma.Cache,
              key: {Device, access_token},
              opts: [ttl: @ttl]
            )
  def get_auth_device_with_account(access_token) do
    Repo.get_by(Device, access_token: access_token)
    |> Repo.preload(account: [:user])
  end

  def add_filter(account = %Account{}, definition) do
    case Ecto.build_assoc(account, :filters)
         |> Filter.create_changeset(%{definition: definition})
         |> Repo.insert() do
      {:ok, filter} ->
        {:ok, filter}

      {:error, changeset} ->
        {:error,
         "add filter failed, with field on error: #{
           Repo.changeset_fields_on_error(changeset)
         }"}
    end
  end

  def get_filter(filter_id) do
    Repo.get(Filter, filter_id)
  end

  @doc """
  get account from repo with the given maxtrix user_id
  Account is preloaded with user
  """
  @spec get_account_by_user_id(String.t()) :: Account.t() | nil
  def get_account_by_user_id(user_id) do
    Repo.one(
      from(a in Account,
        join: u in assoc(a, :user),
        where: u.user_id == ^user_id,
        preload: [:user]
      )
    )
  end

  @doc """
  get account from repo with the given maxtrix user_id and check password
  Account is preloaded with user and devices
  """
  @spec get_account_check_password(String.t(), String.t()) ::
          {:ok, Account.t()}
          | {:error, :login_failed}
          | {:error, :account_not_found}
          | {:error, :wrong_password}
  def get_account_check_password(user_id, password) do
    with account when not is_nil(account) <-
           Repo.one(account_by_user_id_query_with_preload(user_id)) do
      case Argon2.verify_pass(password, account.password_hash) do
        true -> {:ok, account}
        false -> {:error, :wrong_password}
      end
    else
      nil -> {:error, :account_not_found}
    end
  end

  @spec add_device(Account.t(), map) ::
          {:ok, Device.t()} | {:error, Changeset.t()}
  def add_device(%Account{} = account, params) do
    Ecto.build_assoc(account, :devices)
    |> Device.create_changeset(params)
    |> Repo.insert()
  end
end
