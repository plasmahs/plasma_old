defmodule Plasma.Users.User do
  use Plasma.Schema
  alias Plasma.Users.Account

  @type t :: %__MODULE__{
          user_id: String.t(),
          display_name: String.t(),
          avatar_url: String.t(),
          account: String.t()
        }

  schema "users" do
    field :user_id, :string
    field :display_name, :string
    field :avatar_url, :string
    has_one :account, Account
    timestamps()
  end

  def changeset(user, attrs \\ %{}) do
    user
    |> cast(attrs, [:user_id, :display_name, :avatar_url])
    |> validate_required([:user_id])
    |> validate_user_id()
    |> unique_constraint(:user_id, message: "user_id_already_exists")
  end

  # Validate user_id format
  defp validate_user_id(%Ecto.Changeset{valid?: true} = changeset) do
    case changeset do
      %Ecto.Changeset{changes: %{user_id: user_id}} ->
        if Polyjuice.Util.Identifiers.V1.UserIdentifier.valid?(user_id) do
          changeset
        else
          Ecto.Changeset.add_error(changeset, :user_id, "invalid_user_id")
        end

      _ ->
        changeset
    end
  end

  defp validate_user_id(changeset), do: changeset

  def create_changeset(user, attrs \\ %{}), do: changeset(user, attrs)
end
