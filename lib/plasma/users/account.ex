defmodule Plasma.Users.Account do
  use Plasma.Schema
  alias Plasma.Users.User
  alias Plasma.Users.Device
  alias Plasma.Users.Filter

  @type t :: %__MODULE__{
          user: User.t(),
          password: String.t(),
          password_hash: String.t(),
          kind: String.t(),
          devices: [Device.t()],
          filters: [Filter.t()]
        }

  schema "accounts" do
    belongs_to :user, User
    field :password, :string, virtual: true
    field :password_hash, :string
    field :kind, :string
    has_many :devices, Device
    has_many :filters, Filter
    timestamps()
  end

  def changeset(account, attrs \\ %{}) do
    account
    |> cast(attrs, [:kind, :password])
    |> validate_inclusion(:kind, ["user", "guest"])
    |> assoc_constraint(:user)
    |> put_pass_hash()
  end

  def create_changeset(account, attrs \\ %{}), do: changeset(account, attrs)

  defp put_pass_hash(
         %Ecto.Changeset{valid?: true, changes: %{password: password}} =
           changeset
       ) do
    change(changeset, Argon2.add_hash(password))
  end

  defp put_pass_hash(changeset), do: changeset
end
