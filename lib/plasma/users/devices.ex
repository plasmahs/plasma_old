defmodule Plasma.Users.Devices do
  alias Plasma.Users.Device
  alias Plasma.Users.Account
  alias Plasma.Repo
  import Ecto.Query

  @access_token_length Application.get_env(:plasma, :access_token_length)

  def generate_device_id() do
    Polyjuice.Util.Randomizer.unique_id()
  end

  def generate_access_token() do
    Polyjuice.Util.Randomizer.randomize(@access_token_length)
  end

  def update_access_token(%Device{} = device, access_token) do
    Device.update_changeset(device, %{access_token: access_token})
    |> Repo.update()
  end

  def seen(%Device{} = device) do
    case Device.update_changeset(device, %{last_seen: DateTime.utc_now()})
         |> Repo.update() do
      {:ok, device} -> device
      _ -> device
    end
  end

  def get_by_device_id(device_id) do
    Repo.get_by(Device, device_id: device_id)
  end

  def invalidate_access_token(device_id) do
    Repo.get_by(Device, device_id: device_id) |> update_access_token(nil)
  end

  def invalidate_account_access_token(%Account{} = account) do
    from(d in Device,
      join: a in assoc(d, :account),
      where: a.id == ^account.id,
      select: d
    )
    |> Repo.update_all(set: [access_token: nil])
  end
end
