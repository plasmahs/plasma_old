defmodule Plasma.Schema do
  defmacro __using__(_) do
    quote do
      use Ecto.Schema
      @timestamps_opts type: :utc_datetime
      @primary_key {:id, Ecto.UUID, autogenerate: true}
      @foreign_key_type Ecto.UUID

      import Ecto
      import Ecto.Changeset
      import Ecto.Query
    end
  end
end
