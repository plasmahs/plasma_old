defmodule Plasma.Rooms.Room do
  use Plasma.Schema

  schema "rooms" do
    field :room_id, :string
    field :visibility, :string
    field :version, :string
    field :stateset_version, :integer
    timestamps()
  end

  def changeset(room, attrs \\ %{}) do
    room
    |> cast(attrs, [:room_id, :visibility, :version, :stateset_version])
    |> validate_inclusion(:visibility, ["private", "public"])
    |> validate_required([:room_id, :visibility, :version])
    |> unique_constraint(:room_id, message: "room_id_already_exists")
    |> validate_room_id()
  end

  # Validate room_id format
  defp validate_room_id(%Ecto.Changeset{valid?: true} = changeset) do
    case changeset do
      %Ecto.Changeset{changes: %{room_id: room_id}} ->
        if Polyjuice.Util.Identifiers.V1.RoomIdentifier.valid?(room_id) do
          changeset
        else
          Ecto.Changeset.add_error(changeset, :room_id, "invalid_room_id")
        end

      _ ->
        changeset
    end
  end

  def create_changeset(room, attrs \\ %{}), do: changeset(room, attrs)
end
