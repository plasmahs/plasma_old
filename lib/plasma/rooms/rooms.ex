defmodule Plasma.Rooms do
  use Nebulex.Caching
  alias Plasma.Repo
  alias Plasma.Rooms.Room

  def create_room(room_id, version, visibility) do
    insert_room =
      Room.create_changeset(%Room{}, %{
        room_id: room_id,
        version: version,
        visibility: visibility
      })
      |> Repo.insert()

    case insert_room do
      {:ok, filter} ->
        {:ok, filter}

      {:error, changeset} ->
        {:error,
         "create_room failed, with field on error: #{
           Repo.changeset_fields_on_error(changeset)
         }"}
    end
  end

  @ttl :timer.hours(1)
  @decorate cacheable(
              cache: Plasma.Cache,
              key: {Room, room_id},
              opts: [ttl: @ttl]
            )
  def get_room_by_id(room_id) do
    Repo.get(Room, room_id)
  end
end
