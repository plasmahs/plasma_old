defmodule Plasma.Rooms.Event do
  use Plasma.Schema
  alias Plasma.Users.User
  alias Plasma.Rooms.Room
  alias Plasma.Rooms.Event
  alias Plasma.Rooms.PrevEvent
  alias Plasma.Rooms.AuthEvent
  alias Plasma.Rooms.EventContent

  schema "events" do
    field :event_id, :string
    field :state_key, :string
    field :origin, :string
    field :origin_server_ts, :utc_datetime_usec
    field :received_ts, :utc_datetime_usec
    field :type, :string
    belongs_to :sender, User
    belongs_to :room, Room
    has_one :content, EventContent

    many_to_many :prev_events, Event, join_through: PrevEvent
    # join_keys: [event_id: :id, prev_event_id: :id]
    many_to_many :auth_events, Event, join_through: AuthEvent
    # join_keys: [event_id: :id, prev_event_id: :id]

    timestamps()
  end

  def changeset(event, attrs \\ %{}) do
    event
    |> cast(attrs, [:event_id, :state_key, :origin, :origin_server_ts, :type])
    |> validate_required([:type, :origin], message: "required")
    |> validate_origin_server_ts()
  end

  defp validate_origin_server_ts(%Ecto.Changeset{valid?: true} = changeset) do
    case changeset do
      %Ecto.Changeset{changes: %{origin_server_ts: origin_server_ts}}
      when not is_nil(origin_server_ts) ->
        changeset

      _ ->
        Ecto.Changeset.put_change(
          changeset,
          :origin_server_ts,
          DateTime.utc_now()
        )
    end
  end

  defp validate_origin_server_ts(changeset), do: changeset

  def create_changeset(
        event,
        attrs \\ %{},
        %Room{} = room,
        %User{} = sender,
        prev_events,
        auth_events
      ) do
    event
    |> changeset(attrs)
    |> put_assoc(:sender, sender)
    |> put_assoc(:room, room)
    |> put_assoc(:prev_events, prev_events)
    |> put_assoc(:auth_events, auth_events)
  end
end
