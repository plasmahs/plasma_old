defmodule Plasma.RoomServer do
  use GenServer, restart: :transient
  require Logger
  alias Plasma.RoomServer.RoomServerState
  alias Plasma.Rooms.Events
  alias Plasma.Rooms.PDU

  @doc """
  Try to start a room server. If the room is already started, the existing pid is returned
  The room must exists before starting the room server
  """
  def get_room_server(room_id) do
    with room when not is_nil(room) <- Plasma.Rooms.get_room_by_id(room_id),
         {:server_name, server_name} <- Plasma.RuntimeConfig.get_server_name(),
         {:error, {:already_started, pid}} <-
           GenServer.start_link(__MODULE__, {room, server_name},
             name: via_tuple(room_id)
           ) do
      {:ok, pid}
    else
      nil -> {:error, "No room exists in repo with id #{room_id}"}
      {:ok, _} = pid -> pid
    end
  end

  def process_new_event(server, event_id) do
    GenServer.cast(server, {:process_new_event, event_id})
  end

  @impl true
  def handle_cast({:process_new_event, event_id}, state) do
    {:noreply, state}
  end

  @impl true
  def init({room, server_name}) do
    {:ok, %RoomServerState{room: room, server_name: server_name}}
  end

  def via_tuple(room_id),
    do: {:via, Horde.Registry, {Plasma.Registry, "#{__MODULE__}_#{room_id}"}}
end
