defmodule Plasma.Matrix.ClientApi.Plugs.ExtractAccessToken do
  alias Plug.Conn

  @moduledoc """
  ExtractAccessToken plug extract authorization token from query params or HTTP header
  according to (https://matrix.org/docs/spec/client_server/latest.html#using-access-tokens).
  If present, the extracted token is assigned in the current `conn` with `access_token` key.
  """

  @access_token_query "access_token"
  @access_token_header "authorization"

  def init(default), do: default

  def call(conn, _default) do
    # Fetch query params
    conn = Conn.fetch_query_params(conn)
    access_token_param = Map.get(conn.query_params, @access_token_query)

    # find "authorization" in request HTTP headers
    access_token_header =
      conn.req_headers
      |> Enum.find(fn header ->
        case header do
          {h_name, _} -> h_name == @access_token_header
          _ -> false
        end
      end)

    case {access_token_param, access_token_header} do
      {nil, nil} ->
        conn

      {token, nil} ->
        # Token fetched from query params, assign value to connection
        Conn.assign(conn, :access_token, token)

      {nil, {_, token}} ->
        # authorization header found. Look for token in authorization value and assign it to connection
        case Regex.named_captures(~r/Bearer\ (?<token>.+)/, token) do
          %{"token" => token} -> Conn.assign(conn, :access_token, token)
          _ -> conn
        end
    end
  end
end
