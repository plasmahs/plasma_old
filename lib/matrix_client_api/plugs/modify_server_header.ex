defmodule Plasma.Matrix.ClientApi.Plugs.ModifyServerHeader do
  alias Plug.Conn

  def init(_opts), do: Application.spec(:plasma, :vsn) |> to_string()

  def call(conn, version) do
    conn |> Conn.put_resp_header("server", "Plasma/#{version}")
  end
end
