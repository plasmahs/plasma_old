defmodule Plasma.Matrix.ClientApi.Controllers.Errors do
  import Polyjuice.Util.ClientError

  deferror(PlasmaInternalError)
end
