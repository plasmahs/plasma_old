defmodule Plasma.Matrix.ClientApi.Controllers.VersionsController do
  use Plasma.Matrix.ClientApi, :controller

  def get(conn, _params) do
    json(conn, %{versions: ["r0.6.1"]})
  end
end
