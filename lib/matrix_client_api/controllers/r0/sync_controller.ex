defmodule Plasma.Matrix.ClientApi.Controllers.R0.SyncController do
  use Plasma.Matrix.ClientApi, :controller
  import Polyjuice.Util.ClientAPIErrors
  alias Plug.Conn

  def sync(conn, _params) do
    :timer.sleep(20000)
    conn |> json(%{})
  end
end
