defmodule Plasma.Matrix.ClientApi.Controllers.R0.LoginController do
  use Plasma.Matrix.ClientApi, :controller
  alias Plug.Conn
  import Polyjuice.Util.ClientAPIErrors
  alias Plasma.Matrix.ClientApi.Controllers.Tools
  alias Plasma.Users.Accounts
  alias Plasma.Users.Devices
  require Logger

  def get_login(conn, _params) do
    flows =
      [] ++
        if Plasma.RuntimeConfig.config_password_login_enabled?() do
          [%{type: "m.login.password"}]
        else
          []
        end

    json(conn, %{flows: flows})
  end

  def login(conn, %{"type" => "m.login.password"} = params) do
    do_password_login(conn, params)
  end

  def login(conn, %{"type" => login_type}) do
    conn
    |> Conn.put_status(400)
    |> json(MUnkown.new("Bad login type #{login_type}"))
  end

  def logout(conn, _params) do
    with {:ok, device_id} <- Map.fetch(conn.assigns, :device_id),
         device when not is_nil(device) <- Devices.get_by_device_id(device_id),
         _ <- Devices.update_access_token(device, nil) do
      conn |> json(%{})
    else
      _ ->
        conn
        |> Conn.put_status(500)
        |> json(Errors.PlasmaInternalError.new())
    end
  end

  def logout_all(conn, params) do
    with %{auth_device: auth_device} <- conn.assigns,
         _ <- Devices.invalidate_account_access_token(auth_device.account) do
      conn |> json(%{})
    else
      _ ->
        conn
        |> Conn.put_status(500)
        |> json(Errors.PlasmaInternalError.new())
    end
  end

  defp do_password_login(
         conn,
         %{"identifier" => %{"type" => "m.id.user", "user" => user}} = params
       ) do
    with {:server_name, server_name} <- Plasma.RuntimeConfig.get_server_name(),
         {:ok, fq_user_id} <- fq_user_id(user, server_name),
         password <- Map.get(params, "password"),
         {:ok, account} <-
           Accounts.get_account_check_password(fq_user_id, password) do
      # User is authentified
      Logger.debug("User #{account.user.user_id} logged in")

      request_device_id =
        Map.get(params, "device_id", Devices.generate_device_id())

      case Enum.find(account.devices, fn device ->
             device.device_id == request_device_id
           end) do
        # Login with non existing device
        nil ->
          ua = Tools.get_ua(conn)

          initial_display_name =
            Map.get(
              params,
              "initial_display_name",
              "#{ua} on #{ua.os}/#{ua.device}"
            )

          {:ok, new_device} =
            Accounts.add_device(account, %{
              initial_display_name: initial_display_name,
              device_id: Devices.generate_device_id(),
              access_token: Devices.generate_access_token(),
              last_seen: DateTime.utc_now()
            })

          conn
          |> json(%{
            "access_token" => new_device.access_token,
            "user_id" => account.user.user_id,
            "device_id" => new_device.device_id
          })

        # Login with existing device
        device ->
          {:ok, device} =
            Devices.seen(device)
            |> Devices.update_access_token(Devices.generate_access_token())

          conn
          |> json(%{
            "access_token" => device.access_token,
            "user_id" => account.user.user_id,
            "device_id" => device.device_id
          })
      end
    else
      {:error, :invalid_user_id} ->
        conn
        |> Conn.put_status(400)
        |> json(MUnkown.new("Bad user_id format #{user}"))

      {:error, :account_not_found} ->
        conn
        |> Conn.put_status(403)
        |> json(MForbidden.new())

      {:error, :wrong_password} ->
        conn
        |> Conn.put_status(403)
        |> json(MForbidden.new())

      {:error, message} ->
        conn
        |> Conn.put_status(500)
        |> json(Errors.PlasmaInternalError.new(message))
    end
  end

  # Check if the user field is fully qualified. If not, just qualify it with local domain
  defp fq_user_id(user_id, server_name) do
    case Polyjuice.Util.Identifiers.V1.UserIdentifier.parse("@" <> user_id) do
      {:error, _} ->
        case Polyjuice.Util.Identifiers.V1.UserIdentifier.new(
               user_id,
               server_name
             ) do
          {:ok, fqid} -> {:ok, "#{fqid}"}
        end

      {:ok, fqid} ->
        {:ok, "#{fqid}"}
    end
  end
end
