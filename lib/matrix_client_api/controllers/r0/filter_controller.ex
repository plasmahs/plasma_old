defmodule Plasma.Matrix.ClientApi.Controllers.R0.FiltersController do
  use Plasma.Matrix.ClientApi, :controller
  import Polyjuice.Util.ClientAPIErrors
  alias Plasma.Users.Accounts
  alias Plug.Conn

  def upload_filter(conn, %{"user_id" => user_id}) do
    %{auth_device: auth_device} = conn.assigns

    if(auth_device.account.user.user_id != user_id) do
      conn
      |> Conn.put_status(401)
      |> json(MUnauthorized.new("user_id param doesn't match auth user id"))
    else
      case Accounts.add_filter(auth_device.account, conn.body_params) do
        {:ok, filter} ->
          Conn.put_status(conn, 200)
          |> json(%{
            "user_id" => encode_filter_id(auth_device.account.user.user_id)
          })

        {:error, message} ->
          conn
          |> Conn.put_status(500)
          |> json(Errors.PlasmaInternalError.new(message))

        _ ->
          conn
          |> Conn.put_status(500)
          |> json(Errors.PlasmaInternalError.new("Unexpected auth device"))
      end
    end
  end

  def get_filter(conn, %{"user_id" => user_id, "filter_id" => filter_id}) do
    %{auth_device: auth_device} = conn.assigns

    if(auth_device.account.user.user_id != user_id) do
      conn
      |> Conn.put_status(401)
      |> json(MUnauthorized.new("user_id param doesn't match auth user id"))
    else
      with {:ok, decoded_id} <- decode_filter_id(filter_id),
           result when not is_nil(result) <- Accounts.get_filter(decoded_id) do
        conn
        |> Conn.put_status(200)
        |> json(result)
      else
        nil ->
          Conn.send_resp(conn, 404, "")

        {:base64_decode_error} ->
          conn |> Conn.put_status(400) |> json(MUnautMUnrecognized)
      end
    end
  end

  defp encode_filter_id(id) do
    id |> Base.url_encode64(padding: false)
  end

  defp decode_filter_id(encoded) do
    case encoded |> Base.url_decode64(padding: false) do
      {:ok, decoded} -> {:ok, decoded}
      {:error} -> {:base64_decode_error}
    end
  end
end
