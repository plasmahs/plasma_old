defmodule Plasma.Matrix.ClientApi.Controllers.R0.PushRulesController do
  use Plasma.Matrix.ClientApi, :controller
  require Logger

  def get(conn, _params) do
    Logger.warn("GET /_matrix/client/r0/pushrules/ not yet implemented")
    conn |> json(%{})
  end
end
