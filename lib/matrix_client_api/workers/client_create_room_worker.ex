defmodule Plasma.Matrix.ClientApi.Workers.CreateRoomWorker do
  use Oban.Worker, queue: :rooms
  require Logger
  import Plasma.Rooms.EventTypes
  alias Plasma.Rooms
  alias Plasma.Rooms.Events
  alias Plasma.Users

  @impl Oban.Worker
  def perform(%Oban.Job{
        args: %{
          "creator_id" => creator_id,
          "room_id" => room_id,
          "request" => request
        }
      }) do
    with room when not is_nil(room) <- Rooms.get_room_by_id(room_id),
         creator when not is_nil(creator) <- Users.get_user_by_id(creator_id),
         {:server_name, origin} <- Plasma.RuntimeConfig.get_server_name(),
         {:ok, server} <- Plasma.RoomServer.get_room_server(room.id),
         {:ok, %{event: create_event}} <-
           create_m_room_create(
             Map.get(request, "creation_content", %{}),
             room,
             creator,
             origin
           ),
         {:ok, %{event: member_event}} <-
           create_m_room_member(
             Map.get(request, "is_direct", %{}),
             room,
             creator,
             origin
           ) do
      Plasma.RoomServer.process_new_event(server, member_event.id)

      :ok
    else
      {:error, detail} ->
        Logger.warn("Failed to complete room create, #{inspect(detail)}")
        {:error, detail}

      error ->
        Logger.warn("Failed to complete room create, #{inspect(error)}")
        {:error, error}

      nil ->
        {:error,
         "No room or user found with the given job Id: creator_id=#{creator_id}, room_id=#{
           room_id
         }"}
    end
  end

  defp create_m_room_create(event_content, room, creator, origin) do
    %{
      type: m_room_create,
      origin: origin,
      origin_server_ts: DateTime.utc_now(),
      content:
        Map.merge(event_content, %{
          room_version: room.version,
          creator: creator.user_id
        })
    }
    |> Events.create_event(room, creator, [], [])
  end

  defp create_m_room_member(is_direct, room, creator, origin) do
    %{
      type: m_room_create,
      state_key: creator.user_id,
      origin: origin,
      origin_server_ts: DateTime.utc_now(),
      content: %{
        membership: "join",
        is_direct: is_direct
      }
    }
    |> Events.create_event(room, creator, [], [])
  end
end
